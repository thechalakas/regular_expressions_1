﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Regular_Expressions_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //for making comparisons, we have the regex class

            //first let me collect an input from the user
            string input_string = Console.ReadLine();
            //here I using a regular expression to find out if the entered input contains the phrase 'study nildana'
            //remember to never write your own regular expressions. check the notes page for more details
            Match temp_match = Regex.Match(input_string,@"(study nildana)");

            if(temp_match.Success == true)
            {
                Console.WriteLine("Yes, you did include study nildana in your input string. how nice of you!");
            }
            else
            {
                Console.WriteLine("Oh no! you did not include study nildana in your input string. our team will harder to impress you!");
            }


            //lets not let the console dissapear
            Console.ReadLine();
        }
    }
}
